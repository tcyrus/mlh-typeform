declare var $: any;
declare var typeform: any;
declare var settings: any;
declare var moment: any;

const questions = typeform.Form.controls;
const typeformFallback = (settings.browser === 'fallback');

$.fn.keySmash = function() {
	this.keydown().keyup().keypress();
}

window.addEventListener('message', function(event) {
	const { mlh, items: { github, linkedin, devpost } } = event.data;

	// Loop Through All Questions
	questions.forEach(function(question: any) {
		// Get Question Description
		var description: string = question.question.$el.text().trim().toLowerCase();

		// The default action is to skip the question if
		// we don't have an answer
		var answer: string | JQuery = '';
		var input: JQuery;

		switch (question.type) {
			case 'textfield':
				// This is a TextField Question (Normal Input)

				if (/first\s?name/.test(description)) {
					// If the question asks for your first name
					// the answer is probably your first name
					answer = mlh['data']['first_name'];
				} else if (/last\s?name/.test(description)) {
					// If the question asks for your last name
					// the answer is probably your last name
					answer = mlh['data']['last_name'];
				} else if (description.includes('school')) {
					// If the question asks for your school
					// the answer is probably your school
					answer = mlh['data']['school']['name'];
				} else if (description.includes('major')) {
					// If the question asks for your major
					// the answer is probably your major
					answer = mlh['data']['major'];
				} else if (description.includes('dietary restrictions') || description.includes('food allergies')) {
					// If the question asks for your dietary restrictions or food allergies
					if ((mlh['data']['dietary_restrictions'] !== 'None') || question.required) {
						// and you have dietary restrictions or the question is required
						// the answer is probably your dietary restrictions
						answer = mlh['data']['dietary_restrictions'];
					}
				}

				if (answer) {
					// If we have an answer the question
					// Get the <input> (the blank)
					input = (question.$input || question.$wrapper.find('input'));
					// and fill in our possible answer
					input.val(answer).change();
				}

				break;

			case 'date':
				// This is a Date Question (MM/DD/YYYY)

				if (description.includes('birthday') || description.includes('date of birth')) {
					// If the question asks for your birthday
					// The answer is probably the date of birth in the YYYY-MM-DD format
					answer = mlh['data']['date_of_birth'];

					if (!typeformFallback) {
						// If this is a new Typeform
						// Change the answer format to MM / DD / YYYY
						answer = moment(answer, 'YYYY-MM-DD').format('MM / DD / YYYY');
					}
				}

				if (answer) {
					// If we have an answer the question
					// Get the <input> (the blank)
					input = (question.$wrapper || question.$el).find('input');
					// and fill in our possible answer
					// TODO: Setting the Date field doesn't validate in new Typeforms
					input.val(answer).change();
				}

				break;

			case 'email':
				// This is a Email Question (me@example.com)

				if (!description.includes('emergency contact')) {
					// If the question doesn't ask for an emergency contact
					// the answer is probably your email
					answer = mlh['data']['email'];
				}

				if (answer) {
					// If we have an answer the question
					// Get the <input> field
					input = question.$input || question.$wrapper.find('input');
					// and fill in our possible answer
					input.val(answer).change();
				}

				break;

			case 'number':
				// This is a Number Question (555-555-5555)

				if (description.includes('phone number') && !description.includes('emergency contact')) {
					// If the question asks for your phone number
					// and doesn't ask for an emergency contact
					// the answer is probably your phone number
					answer = mlh['data']['phone_number'];
				}

				if (answer) {
					// If we have an answer the question
					// Get the <input> field
					input = question.$input || question.$wrapper.find('input');
					// and fill in our possible answer
					input.val(mlh['data']['phone_number']).change();
				}

				break;

			case 'website':
				// This is a Website Question (https://example.com)

				if (description.includes('github')) {
					// If the question asks for your GitHub
					// the answer is probably your GitHub Profile URL
					answer = github;
				} else if (description.includes('linkedin')) {
					// If the question asks for your LinkedIn
					// the answer is probably your LinkedIn Profile URL
					answer = linkedin;
				} else if (description.includes('devpost')) {
					// If the question asks for your Devpost
					// the answer is probably your Devpost Profile URL
					answer = devpost;
				}

				// If we have an answer the question
				if (answer) {
					// Get the <input> field
					let input = question.$input || question.$wrapper.find('input');
					// and fill in our possible answer
					input.val(answer).change();
				}

				break;

			case 'list':
				// This is a List Question

				if (description.includes('gender')) {
					// If the question is about gender
					// Get all the choices (<li>)
					var choices: JQuery = (question.$ul || question.$wrapper).find('li.choice, .control > .columns>li');

					// Loop through each choice
					choices.each(function() {
						// Get Choice Label
						var choiceText: JQuery = $(this).find('.label, label').text().trim().toLowerCase();

						// Check if Choice matches up with the User Info
						if (choiceText.includes('female')) {
							if (mlh['data']['gender'] === 'Female') {
								// this is probably the answer
								answer = $(this);
								// don't look at the other choices
								return false;
							}
						} else if (choiceText.includes('male')) {
							if (mlh['data']['gender'] === 'Male') {
								// this is probably the answer
								answer = $(this);
								// don't look at the other choices
								return false;
							}
						} else if (/non[-\s]binary/.test(choiceText)) {
							if (mlh['data']['gender'] === 'Non-binary') {
								// this is probably the answer
								answer = $(this);
								// don't look at the other choices
								return false;
							}
						}
					});
				}

				// If we have an answer the question
				if (answer) {
					if (typeformFallback) {
						// If this is an old Typeform
						// Fill it in normally
						answer.find('input').click();
					} else {
						// If this is a new Typeform
						// Do fancy selection for the answer
						question.select(answer);
					}
				}

				break;
		}
	});
});
