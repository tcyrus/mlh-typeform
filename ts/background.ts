declare var chrome: any;

chrome.browserAction.onClicked.addListener((tab: any) => {
	const CLIENT_ID = '3493640f26332b39805a23ff5f491d4ba76b9c852bbb952dc085900c2963989c';
	const REDIRECT_URI = chrome.identity.getRedirectURL('provider_cb');
	const AUTH_URL = `https://my.mlh.io/oauth/authorize?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=token`;

	chrome.identity.launchWebAuthFlow({url: AUTH_URL, interactive: true}, (redirect_url: string) => {
		const ACCESS_TOKEN = redirect_url.split('access_token=')[1];
		const xhr = new XMLHttpRequest();

		xhr.onload = () => {
			if (this.status === 401) return;

			const mlh = JSON.parse(xhr.responseText);

			chrome.storage.sync.get({
				github: 'https://github.com/',
				linkedin: 'https://www.linkedin.com/in/',
				devpost: 'http://devpost.com/'
			}, (items: any) => {
				chrome.tabs.executeScript(tab.id, {file: 'js/contentScript.js'}, () => {
					chrome.tabs.sendMessage(tab.id, {mlh: mlh, items: items});
				});
			});
		};

		xhr.open('GET', `https://my.mlh.io/api/v1/user?access_token=${ACCESS_TOKEN}`);
		xhr.send();
	});
});
