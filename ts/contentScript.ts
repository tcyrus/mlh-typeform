declare var chrome: any;

function injectScript(file: string, callback: any): void {
	let s = document.createElement('script');
	document.body.appendChild(s);
	s.setAttribute('type', 'text/javascript');
	s.onload = callback;
	s.src = chrome.extension.getURL(file);
}

chrome.runtime.onMessage.addListener((msg: any) => {
	injectScript('js/autofill.js', () => {
		window.postMessage(msg, window.location.origin);
	});
});