declare var chrome: any;

// Saves options to chrome.storage.sync.
function saveOptions(): void {
	chrome.storage.sync.set({
		github: (<HTMLInputElement>document.getElementById('github')).value,
		linkedin: (<HTMLInputElement>document.getElementById('linkedin')).value,
		devpost: (<HTMLInputElement>document.getElementById('devpost')).value
	}, () => {
		// Update status to let user know options were saved.
		var status = document.getElementById('status');
		status.textContent = 'Options saved';
		setTimeout(() =>
			status.textContent = ''
		, 750);
	});
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restoreOptions(): void {
	// Use default value for URLs
	chrome.storage.sync.get({
		github: 'https://github.com/',
		linkedin: 'https://www.linkedin.com/in/',
		devpost: 'http://devpost.com/'
	}, (items: any) => {
		(<HTMLInputElement>document.getElementById('github')).value = items.github
		(<HTMLInputElement>document.getElementById('linkedin')).value = items.linkedin
		(<HTMLInputElement>document.getElementById('devpost')).value = items.devpost
	});
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.getElementById('save').addEventListener('click', saveOptions);