.PHONY: key extensionKey extensionID

key:
	2>/dev/null openssl genrsa 2048 | openssl pkcs8 -topk8 -nocrypt -out key.pem

extensionKey:
	2>/dev/null openssl rsa -in key.pem.not -pubout -outform DER | openssl base64 -A

extensionID:
	2>/dev/null openssl rsa -in key.pem.not -pubout -outform DER | shasum -a 256 | head -c32 | tr 0-9a-f a-p