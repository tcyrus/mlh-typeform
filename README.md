# MLH Typeform Automation

## Description
This makes use of the MyMLH API to fill in some of the fields on a Typeform.

## Why Typeform?
The majority of hackathon signups that I have come across use Typeform, and it is the hardest to Autofill.

## Notes
If the Typeform is setup in an unusual or non-standard way (e.g. `TextField` is used for email input instead of `Email`), the code will skip it. The code may not work for all MLH supported fields (e.g. `Date`). To get the results, you should try using the fallback form (just add `/fallback` to the end of the Typeform URL).

I have contacted Typeform and have confirmed that this does not violate their Terms and Conditions (Support Request #43352)