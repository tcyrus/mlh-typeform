var _this = this;
var questions = typeform.Form.controls;
var typeformFallback = (settings.browser === 'fallback');
$.fn.keySmash = function () {
    _this.keydown().keyup().keypress();
};
window.addEventListener('message', function (event) {
    var _a = event.data, mlh = _a.mlh, _b = _a.items, github = _b.github, linkedin = _b.linkedin, devpost = _b.devpost;
    questions.forEach(function (question) {
        var description = question.question.$el.text().trim().toLowerCase();
        var answer = '';
        switch (question.type) {
            case 'textfield':
                if (/first\s?name/.test(description)) {
                    answer = mlh['data']['first_name'];
                }
                else if (/last\s?name/.test(description)) {
                    answer = mlh['data']['last_name'];
                }
                else if (description.includes('school')) {
                    answer = mlh['data']['school']['name'];
                }
                else if (description.includes('major')) {
                    answer = mlh['data']['major'];
                }
                else if (description.includes('dietary restrictions') || description.includes('food allergies')) {
                    if ((mlh['data']['dietary_restrictions'] !== 'None') || question.required) {
                        answer = mlh['data']['dietary_restrictions'];
                    }
                }
                if (answer) {
                    var input = (question.$input || question.$wrapper.find('input'));
                    input.val(answer).change();
                }
                break;
            case 'date':
                if (description.includes('birthday') || description.includes('date of birth')) {
                    answer = mlh['data']['date_of_birth'];
                    if (!typeformFallback) {
                        answer = moment(answer, 'YYYY-MM-DD').format('MM / DD / YYYY');
                    }
                }
                if (answer) {
                    var input = (question.$wrapper || question.$el).find('input');
                    input.val(answer).change();
                }
                break;
            case 'email':
                if (!description.includes('emergency contact')) {
                    answer = mlh['data']['email'];
                }
                if (answer) {
                    var input = question.$input || question.$wrapper.find('input');
                    input.val(answer).change();
                }
                break;
            case 'number':
                if (description.includes('phone number') && !description.includes('emergency contact')) {
                    answer = mlh['data']['phone_number'];
                }
                if (answer) {
                    var input = question.$input || question.$wrapper.find('input');
                    input.val(mlh['data']['phone_number']).change();
                }
                break;
            case 'website':
                if (description.includes('github')) {
                    answer = github;
                }
                else if (description.includes('linkedin')) {
                    answer = linkedin;
                }
                else if (description.includes('devpost')) {
                    answer = devpost;
                }
                if (answer) {
                    var input = question.$input || question.$wrapper.find('input');
                    input.val(answer).change();
                }
                break;
            case 'list':
                if (description.includes('gender')) {
                    var choices = (question.$ul || question.$wrapper).find('li.choice, .control>.columns>li');
                    choices.each(function () {
                        var choiceText = $(this).find('.label, label').text().trim().toLowerCase();
                        if (choiceText.includes('female')) {
                            if (mlh['data']['gender'] === 'Female') {
                                answer = $(this);
                                return false;
                            }
                        }
                        else if (choiceText.includes('male')) {
                            if (mlh['data']['gender'] === 'Male') {
                                answer = $(this);
                                return false;
                            }
                        }
                        else if (/non[-\s]binary/.test(choiceText)) {
                            if (mlh['data']['gender'] === 'Non-binary') {
                                answer = $(this);
                                return false;
                            }
                        }
                    });
                }
                if (answer) {
                    if (typeformFallback) {
                        answer.find('input').click();
                    }
                    else {
                        question.select(answer);
                    }
                }
                break;
        }
    });
});
