function saveOptions() {
    chrome.storage.sync.set({
        github: document.getElementById('github').value,
        linkedin: document.getElementById('linkedin').value,
        devpost: document.getElementById('devpost').value
    }, function () {
        var status = document.getElementById('status');
        status.textContent = 'Options saved';
        setTimeout(function () {
            return status.textContent = '';
        }, 750);
    });
}
function restoreOptions() {
    chrome.storage.sync.get({
        github: 'https://github.com/',
        linkedin: 'https://www.linkedin.com/in/',
        devpost: 'http://devpost.com/'
    }, function (items) {
        document.getElementById('github').value = items.github(document.getElementById('linkedin')).value = items.linkedin(document.getElementById('devpost')).value = items.devpost;
    });
}
document.addEventListener('DOMContentLoaded', restoreOptions);
document.getElementById('save').addEventListener('click', saveOptions);
