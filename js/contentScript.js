function injectScript(file, callback) {
    var s = document.createElement('script');
    document.body.appendChild(s);
    s.setAttribute('type', 'text/javascript');
    s.onload = callback;
    s.src = chrome.extension.getURL(file);
}
chrome.runtime.onMessage.addListener(function (msg) {
    injectScript('js/autofill.js', function () {
        window.postMessage(msg, window.location.origin);
    });
});
