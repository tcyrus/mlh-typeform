var _this = this;
chrome.browserAction.onClicked.addListener(function (tab) {
    var CLIENT_ID = '3493640f26332b39805a23ff5f491d4ba76b9c852bbb952dc085900c2963989c';
    var REDIRECT_URI = chrome.identity.getRedirectURL('provider_cb');
    var AUTH_URL = "https://my.mlh.io/oauth/authorize?client_id=" + CLIENT_ID + "&redirect_uri=" + REDIRECT_URI + "&response_type=token";
    chrome.identity.launchWebAuthFlow({ url: AUTH_URL, interactive: true }, function (redirect_url) {
        var ACCESS_TOKEN = redirect_url.split('access_token=')[1];
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            if (_this.status === 401)
                return;
            var mlh = JSON.parse(xhr.responseText);
            chrome.storage.sync.get({
                github: 'https://github.com/',
                linkedin: 'https://www.linkedin.com/in/',
                devpost: 'http://devpost.com/'
            }, function (items) {
                chrome.tabs.executeScript(tab.id, { file: 'js/contentScript.js' }, function () {
                    chrome.tabs.sendMessage(tab.id, { mlh: mlh, items: items });
                });
            });
        };
        xhr.open('GET', "https://my.mlh.io/api/v1/user?access_token=" + ACCESS_TOKEN);
        xhr.send();
    });
});
